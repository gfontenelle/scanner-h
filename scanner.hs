import Data.Char
import Data.List


-- Palavras reservadas
reservedWord :: String -> (String, String)
reservedWord [] = ([], [])
reservedWord xs 
    | isPrefixOf "do" xs && (if length xs == 2 then True else notElem (xs!! 2) char) = (take 2 xs ++ " => tokenCode = 20\n", drop 2 xs) 
    | isPrefixOf "if" xs && (if length xs == 2 then True else notElem (xs!! 2) char) = (take 2 xs ++ " => tokenCode = 21\n", drop 2 xs) 
    | isPrefixOf "or" xs && (if length xs == 2 then True else notElem (xs!! 2) char) = (take 2 xs ++ " => tokenCode = 22\n", drop 2 xs) 
    | isPrefixOf "and" xs && (if length xs == 3 then True else notElem (xs!! 3) char) = (take 3 xs ++ " => tokenCode = 23\n", drop 3 xs) 
    | isPrefixOf "int" xs && (if length xs == 3 then True else notElem (xs!! 3) char) = (take 3 xs ++ " => tokenCode = 24\n", drop 3 xs) 
    | isPrefixOf "for" xs && (if length xs == 3 then True else notElem (xs!! 3) char) = (take 3 xs ++ " => tokenCode = 25\n", drop 3 xs) 
    | isPrefixOf "new" xs && (if length xs == 3 then True else notElem (xs!! 3) char) = (take 3 xs ++ " => tokenCode = 26\n", drop 3 xs) 
    | isPrefixOf "not" xs && (if length xs == 3 then True else notElem (xs!! 3) char) = (take 3 xs ++ " => tokenCode = 27\n", drop 3 xs) 
    | isPrefixOf "nil" xs && (if length xs == 3 then True else notElem (xs!! 3) char) = (take 3 xs ++ " => tokenCode = 28\n", drop 3 xs) 
    | isPrefixOf "else" xs && (if length xs == 4 then True else notElem (xs!! 4) char) = (take 4 xs ++ " => tokenCode = 29\n", drop 4 xs) 
    | isPrefixOf "void" xs && (if length xs == 4 then True else notElem (xs!! 4) char) = (take 4 xs ++ " => tokenCode = 30\n", drop 4 xs) 
    | isPrefixOf "float" xs && (if length xs == 5 then True else notElem (xs!! 5) char) = (take 5 xs ++ " => tokenCode = 31\n", drop 5 xs) 
    | isPrefixOf "while" xs && (if length xs == 5 then True else notElem (xs!! 5) char) = (take 5 xs ++ " => tokenCode = 32\n", drop 5 xs) 
    | isPrefixOf "return" xs && (if length xs == 6 then True else notElem (xs!! 6) char) = (take 6 xs ++ " => tokenCode = 33\n", drop 6 xs) 
    | isPrefixOf "string" xs && (if length xs == 6 then True else notElem (xs!! 6) char) = (take 6 xs ++ " => tokenCode = 34\n", drop 6 xs) 
    | otherwise = ([], xs)
    where char = ['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9']
 
-- Simbolos reservados
symbol :: String -> (String, String)
symbol [] = ([],[])
symbol (x:xs)
    | x == '<' && (take 1 xs) == "=" = (x:take 1 xs ++ " => tokenCode = 17\n", drop 1 xs)
    | x == '>' && (take 1 xs) == "=" = (x:take 1 xs ++ " => tokenCode = 18\n", drop 1 xs)
    | x == '!' && (take 1 xs) == "=" =  (x:take 1 xs ++ " => tokenCode = 19\n", drop 1 xs)
    | x == '+' = (x:" => tokenCode = 1\n", xs)
    | x == '-' = (x:" => tokenCode = 2\n", xs)
    | x == '*' = (x:" => tokenCode = 3\n", xs)
    | x == '/' = (x:" => tokenCode = 4\n", xs)
    | x == '=' = (x:" => tokenCode = 5\n", xs)
    | x == '(' = (x:" => tokenCode = 6\n", xs)
    | x == ')' = (x:" => tokenCode = 7\n", xs)
    | x == '[' = (x:" => tokenCode = 8\n", xs)
    | x == ']' = (x:" => tokenCode = 9\n", xs)
    | x == '{' = (x:" => tokenCode = 10\n", xs)
    | x == '}' = (x:" => tokenCode = 11\n", xs)
    | x == '<' = (x:" => tokenCode = 12\n", xs)
    | x == '>' = (x:" => tokenCode = 13\n", xs)
    | x == ',' = (x:" => tokenCode = 14\n", xs)
    | x == ';' = (x:" => tokenCode = 15\n", xs)
    | x == '.' = (x:" => tokenCode = 16\n", xs)
    | otherwise = ([], x:xs)
    where 
           allowed = ['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9']

scanToken :: String -> (String, String)
scanToken [] = ([],[])
scanToken xs
    | reserved /= [] = reservedWord xs
    | s /= [] = symbol xs
    | otherwise = ([], xs)
    where 
        (reserved, y)  = reservedWord xs
        (s, ys) = symbol xs
    
-- Identificação de número
-- Retorna o tamanho de uma string considerada número.
internalNumber :: String -> Int -> Int -> [Int]
internalNumber [] len z = [len,z]
internalNumber ('.':ys) 0 0 = [0,0]
internalNumber (y:ys) len z
    | y `elem` l = internalNumber ys (len + 1) z
    | y == '.' && z < 1 && head ys `elem` l = internalNumber ys (len + 1) (z + 1)
    | y == '.' = [len, z]
    | otherwise = [len, z]
    where 
         l = ['0'..'9']
         
scanNumber :: String -> (String, String)
scanNumber [] = ([], [])
scanNumber ys
    | len > 0 && point == 0 = (take len ys ++ " => tokenCode = 36, value = " ++ take len ys ++ "\n", drop len ys)
    | len > 0 && point > 0 = (take len ys ++ " => tokenCode = 37, value = " ++ take len ys ++ "\n", drop len ys)
    | otherwise = ([], ys)
   where
        len = internalNumber ys 0 0 !! 0
        point = internalNumber ys 0 0 !! 1
        
-- Fim da identificação de número 
           
-- Tratamento de Strings.
internalScanString :: Bool -> String -> String -> (String, String)
-- String começa mas não termina
internalScanString isSimple [] acc = ((acc ++ " -- [malformed string]\n"), [])

-- String é o último token do arquivo.
internalScanString isSimple [x] acc
 | ((isSimple && x == '\'') || ((not isSimple) && x == '"')) = ((acc ++ [x] ++ "\n"), [])
 | otherwise = internalScanString isSimple [] (acc++[x]) -- Cai no caso em que String começa mas não termina

-- String acaba mas o jogo continua.
internalScanString isSimple (x:xs) acc 
 | ((isSimple && x == '\'') || ((not isSimple) && x == '"')) = ((acc ++ [x] ++ " => tokenCode = 35, value = " ++ tail acc ++ "\n"), xs)
 | otherwise = internalScanString isSimple xs (acc++[x]) -- String não acabou ainda.

scanString :: String -> (String, String)
scanString [] = ([], [])
scanString [x] = ([], [x])
scanString (x:xs)
 | (x == '\'') = internalScanString True xs ['\'']
 | (x == '"') = internalScanString False xs ['"']
 | otherwise = ([], [x] ++ xs)    
-- Fim do tratamento de Strings.

{-
Remoção de comentários no começo da String.
Entrada: 
    Bool -> se deve procurar por um "/*" ou se já leu o "/*" e deve procurar por um "*/".
    String -> String para remover o comentário.
Saída:
    String -> A mesma String de entrada com o comentário removido e sem caracteres em branco após ele, se houver.
-}
internalRemoveComments :: Bool -> String -> String -> (String, String)
internalRemoveComments isStart [] acc
 | (isStart) = (acc, [])
 | otherwise = (acc ++ "-- [malformed comment]\n", [])
internalRemoveComments isStart [x] acc
 | (isStart) = (acc, [x])
 | otherwise = (acc ++ "-- [malformed comment]\n", [])
internalRemoveComments isStart xs acc
 | (isStart) && ((xs !! 0) == '/') && ((xs !! 1) == '*') = internalRemoveComments False (drop 2 xs) (acc ++ "/*")
 | (not isStart) && ((xs !! 0) /= '*') = internalRemoveComments isStart (tail xs) (acc ++ [head xs])
 | (not isStart) && ((xs !! 0) == '*') && ((xs !! 1) == '/') = (acc ++ "*/", drop 2 xs)
 | otherwise = (acc, xs)

removeComments :: String -> (String, String)
removeComments xs = internalRemoveComments True xs []
-- Fim da remoção de comentários.

{-
Remoção de caracteres em branco no começo da String.
Entrada:
    String -> String para remover os caracteres em branco.
Saída:
    String -> A mesma String de entrada sem os caracteres em branco do início.    
-}
removeWhitespace :: String -> String
removeWhitespace [] = []
removeWhitespace (x:xs)
 | (isSpace x) = removeWhitespace xs
 | otherwise = [x] ++ xs
-- Fim da remoção de caracteres em branco.

{-
-1 - remover comentários e whitespace
-2 - scanstring
-3 - int/float
-4 tokens
    
-}
buildFile :: Int -> String -> (String, String) -> String
-- Caso base, terminou de montar a String a ser escrita.
buildFile 0 str (a, b) = str

-- Início, tenta remover comentários.
buildFile 1 str (a, b) = buildFile 2 str (removeComments $ removeWhitespace b)

-- Tratamento de comentários e início de String.
buildFile 2 str (a, b)
 | (null a) = buildFile 3 str (scanString b) -- Não leu nenhum comentário, então vamos tentar fazer parse de String.
 | otherwise = buildFile 1 str ([], b) -- Removeu comentários, então vamos começar de novo
-- Se leu string, começa do zero, Senão faz parte de Int/Float

-- Tratamento de String e início de Int/Float.
buildFile 3 str (a, b) 
 | (null a) = buildFile 4 str (a, b) -- parse int/float -- Não leu nenhuma String, então vamos tentar fazer parse de Number.
 | otherwise = buildFile 1 (str++a) ([], b) -- Leu uma String, então vamos começar de novo.

-- Tratamento de Int/Float e início de Tokens.
buildFile 4 str (a, b)
 | (null a) = buildFile 5 str (a, b) -- parse token -- Não leu nenhum Number, então vamos tentar fazer parse de Token.
 | otherwise = buildFile 1 (str++a) ([], b) -- Leu um Number, então vamos começar de novo.

-- Tratamento de Tokens e possível fim, ou não.
buildFile 5 str (a, b)
 | ((null a) && (null b)) = buildFile 0 str (a, b) -- Fim do arquivo, nada mais para ler.
 | otherwise = buildFile 1 (str++a) ([], b) -- O arquivo não acabou, vamos começar o procedimento todo de novo.

main = do
    putStr ("Nome do arquivo de entrada: ")
    inputFileName <- getLine
    putStr ("Nome do arquivo de saída: ")
    outputFileName <- getLine

    inputFile <- readFile inputFileName
    writeFile outputFileName ("") -- Cria um arquivo limpo ou sobrescreve um já existente
    -- scan inputFile outputFileName    
    appendFile outputFileName (fst $ scanString $ snd $ removeComments $ removeWhitespace inputFile)    
    -- appendFile outputFileName (buildFile)    
    putStr ("Concluído com sucesso!\n")